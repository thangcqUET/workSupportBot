FROM node:14.18.0-alpine
ENV NODE_ENV=production
WORKDIR /app
COPY ["package.json", "package-lock.json*", "./"]
RUN npm install -g npm@8.2.0
RUN npm install --production
COPY . .
CMD [ "npm" , "start"]