const nodeSchedule = require('node-schedule');

class LoggingTask {
  /**
   *
   * @param {object} params
   * @param {Telegraf} [params.bot]
   * @param {MongoDBConnection} [params.mongoDBConnection]
   */
  constructor(params) {
    this.bot = params.bot;
    this.mongoDBConnection = params.mongoDBConnection;
  }

  async init() {
    this.bot.command('log', async (ctx) => {
      const { text } = ctx.message;
      const log = text.split('/log')[1].trim();
      const timestamp = ctx.message.date;
      const detail = '';
      const telegramUserId = ctx.message.from.id;
      const taskLogModel = this.mongoDBConnection.exportTaskLogModel();
      await taskLogModel.create({
        userId: telegramUserId,
        timestamp,
        content: log,
        detail,
      });
    });
    const rule = new nodeSchedule.RecurrenceRule();
    rule.dayOfWeek = [1, 2, 3, 4, 5];
    rule.hour = [8, 10, 14, 16, 17];
    rule.minute = 30;
    // rule.hour = [17, 18, 19, 20, 21, 22, 23];
    // rule.minute = [10, 20, 30, 40, 50, 59];
    nodeSchedule.scheduleJob(rule, () => {
      this.loggingRemind();
    });
  }

  async loggingRemind() {
    const userModel = this.mongoDBConnection.exportUserModel();
    const users = await userModel.find({});
    for (const user of users) {
      const teleId = user.userId;
      this.bot.telegram.sendMessage(teleId, 'What are you doing?');
    }
  }
}
module.exports = {
  LoggingTask,
};
