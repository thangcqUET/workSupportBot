class SignUp {
  constructor(params) {
    this.bot = params.bot;
    this.mongoDBConnection = params.mongoDBConnection;
  }

  async init() {
    this.bot.start(async (ctx) => {
      // add non-exist user
      const telegramUserId = ctx.message.from.id;
      const name = `${ctx.message.from.last_name} ${ctx.message.from.first_name}`;
      const { username } = ctx.message.from;
      const userModel = this.mongoDBConnection.exportUserModel();
      const query = {
        userId: telegramUserId,
      };
      const update = {
        userId: telegramUserId,
        name,
        username,
      };
      const options = { upsert: true, new: true, rawResult: true };
      const res = await userModel.findOneAndUpdate(query, update, options);
      if (res.lastErrorObject.updatedExisting) {
        ctx.reply('Your account already exists');
      } else {
        ctx.reply('Sign Up Success');
      }
    });
    this.bot.command('stop', async (ctx) => {
      const telegramUserId = ctx.message.from.id;
      const userModel = this.mongoDBConnection.exportUserModel();
      const query = {
        userId: telegramUserId,
      };
      await userModel.findOneAndRemove(query);
      ctx.reply('Remove account');
    });
  }
}
module.exports = {
  SignUp,
};
