eval $(ssh-agent -s)
echo "$DEV_SSH_PRIVATE_KEY" | tr -d '\r' | ssh-add -
mkdir -p ~/.ssh
chmod 700 ~/.ssh
touch ~/.ssh/known_hosts
ssh-keyscan $DEV_IP >> ~/.ssh/known_hosts
ssh ubuntu@$DEV_IP touch /app/.env
scp $DEV_ENV ubuntu@$DEV_IP:/app/.env
scp docker-compose.yml ubuntu@$DEV_IP:/app/
ssh ubuntu@$DEV_IP "cd /app && docker-compose --env-file .env down"
ssh ubuntu@$DEV_IP "cd /app && docker-compose --env-file .env up -d"